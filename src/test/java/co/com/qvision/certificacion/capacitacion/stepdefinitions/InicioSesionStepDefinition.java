package co.com.qvision.certificacion.capacitacion.stepdefinitions;

import co.com.qvision.certificacion.capacitacion.tasks.AbrirElNavegador;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;

public class InicioSesionStepDefinition {

    @Before
    public void inicio() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Dado("^que (.*) se encuentra en la pagina ORANGE$")
    public void queJenniSeEncuentraEnLaPaginaOrange(String nombreActor) {
        theActorCalled(nombreActor).wasAbleTo(AbrirElNavegador.enLaPaginaDeOrange());
    }

    @Cuando("^ella ingresa sus datos de usuario prueba$")
    public void ellaIngresaSusDatosDeUsuarioPrueba(DataTable args) {

    }

    @Entonces("^ella debería ver las opciones de OrangeHRM$")
    public void ellaDeberiaVerLasOpcionesDeOrangeHrm() {

    }


}
