#language: es
#author: Jenniffer Escalante

Característica:Logueo con la pagina web OrangeHRM
  Como un usuario
  Quiero ingresar a OrangeHRM
  A personalizar mis datos de contacto

  @LogeoOrange
  Esquema del escenario: Logueo de usuario en la pagina OrangeHRM
    Dado que jenni se encuentra en la pagina ORANGE
    Cuando ella ingresa sus datos de usuario prueba
      | user   | password   |
      | <user> | <password> |
    Entonces ella debería ver las opciones de OrangeHRM

    Ejemplos:
      | user  | password |
      | Admin | admin123 |
